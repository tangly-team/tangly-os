---
title: "ADR"
linkTitle: "ADR"
weight: 80
---

The [Architecture Design Records](https://en.wikipedia.org/wiki/Architectural_decision) _ADR_ for the overall open source products of tangly.

These design decisions often define how we want to develop applications in Modern Java.
