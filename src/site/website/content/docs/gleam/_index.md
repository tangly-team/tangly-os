---
title: "Gleam - Library to import and export domain objects to TSV and JSON data files"
linkTitle: "Gleam"
weight: 35
---

The readme, user manual, documentation of the component and architecture consideration of the components.

The API documentation can be found under [Javadoc](/docs/gleam/api-gleam/index.html).


