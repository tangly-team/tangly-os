---
title: "Collaborators - Collaborators Bounded Domain"
linkTitle: "Collaborators"
weight: 20
---

The readme, user manual, documentation of the component and architecture consideration of the components.

The API documentation can be found under [Javadoc](/docs/domains/collaborators/api-collaborators/index.html).
