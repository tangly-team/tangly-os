---
title: "Agile - Agile Bounded Domain"
linkTitle: "Agile"
weight: 10
---

The readme, user manual, documentation of the component and architecture consideration of the components.

The API documentation can be found under [Javadoc](/docs/domains/agile/api-agile/index.html).
