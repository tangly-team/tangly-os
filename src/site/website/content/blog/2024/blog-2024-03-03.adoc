---
title: "Asciidoc and Hugo Website Improvements"
linkTitle: "Website Improvements"
date: 2024-03-25
tags: ["hugo"]
---

include::../fragments/_config-mb-blog.aodc[]

image::2024-03-03-head.png[width=420,height=360,role=left]

== Include Common Blocks

== Bibliography

== Taxonomies

== RevealJS

== Lessons Learnt

== Links

include::../fragments/_hugo-links.adoc[]
