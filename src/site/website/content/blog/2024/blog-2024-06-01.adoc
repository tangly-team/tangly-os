---
title: "Daily Scrum Questions"
linkTitle: "Daily Scrum Questions"
date: 2024-06-01
tags: ["agile"]
---

include::../fragments/_config-mb-blog.aodc[]

image::2024-06-01-head.jpg[width=420,height=360,role=left]

== Daily Scrum

The official definition of the Daily Scrum as stated in the {ref-scrumguide} is:

_The purpose of the Daily Scrum is *to inspect progress toward the Sprint Goal* and adapt the Sprint Backlog as necessary, adjusting the upcoming planned work._

_The Daily Scrum is a 15-minute event for the Developers of the Scrum Team._
_To reduce complexity, it is held at the same time and place every working day of the Sprint._
_If the Product Owner or Scrum Master are actively working on items in the Sprint Backlog, they participate as Developers._

_The Developers can select *whatever structure and techniques they want*, as long as their Daily Scrum focuses on progress toward the Sprint Goal and produces an actionable plan for the next day of work._
_This creates focus and improves self-management._

_Daily Scrums improve communications, identify impediments, promote quick decision-making, and consequently eliminate the need for other meetings._

_The Daily Scrum is *not* the only time Developers are allowed to adjust their plan._
_*They often meet throughout the day for more detailed discussions about adapting or re-planning the rest of the Sprint’s work.*_

== Daily Scrum Questions

. Can we achieve the Sprint Goal?
. Do we have to change the content of the Sprint Backlog to achieve the Sprint Goal?
. Who needs help?
. Who will pair with me today?
. Where do we need to swarm today?
. Do we have any blockers?
. What should we drop to focus on the highest priority?
. Shall we reduce WIP?
. Appreciation time for provided help. Why wait until the Retrospective?
. What were the biggest learnings in this Sprint?
. Start with a quote about software engineering or agile!

== Lessons Learnt

A great Scrum master shall make his teams aware of the true goal of a daily Scrum meeting
cite:[things-every-scrum-practitioner-should-know,scrum-pocket-guide-3rd,great-scrum-master,zombie-scrum-survival-guide,mastering-professional-scrum].

Avoid doing Scrum, ask yourself how to be agile and continuously improve.

Motivate your collaborators to read the {ref-scrumguide}.
Support continuous formation.
Certification does not proof deep knowledge.
But at least you have learnt the definition and read historical and important texts.

[bibliography]
== Links

- [[[detect-fake-scrum,1]]] link:../../2022/how-to-detect-fake-scrum/[How to Detect Fake Scrum?].
Marcel Baumann. 2022.
- [[[doing-agile,2]]] link:../../2022/doing-agile/[Doing Agile].
Marcel Baumann. 2022.
- [[[scrum-developer-formation,3]]] link:../../2021/scrum-developer-formation/[Scrum Developer Formation].
Marcel Baumann. 2021.
- [[[detecting-agile-bullshit,4]]] link:../../2019/detecting-agile-bullshit/[Detecting Agile Bullshit].
Marcel Baumann. 2019.
- [[[pragmatic-craftsmanship,5]]] link:../../2018/pragmatic-craftsmanship-professional-software-developer/[Pragmatic Craftsmanship Professional Software Developer].
Marcel Baumann. 2019.
- [[[do-not-need-to-do-in-scrum,6]]] link:../../2016/what-you-do-not-need-to-do-in-scrum/[What You Do Not Need To Do In Scrum].
Marcel Baumann. 2016.

== References

bibliography::[]
