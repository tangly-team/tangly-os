/*
 * Copyright 2006-2022 Marcel Baumann
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 *          https://apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.tangly.gleam.model;

import java.util.List;
import java.util.Optional;

public interface Entity<T> {
    enum Cardinality {
        zero_one, one_one, zero_m, one_m
    }

    record Attribute<T>(String name, Class<T> type, Cardinality cardinality, String description) {
    }

    record Relation<T>(String name, Entity<T> type, Cardinality cardinality, String description) {
    }

    Class<T> identity();

    List<Entity<?>> references();

    Optional<JsonEntity<T>> json();

    Optional<TsvEntity<T>> tsv();
}
